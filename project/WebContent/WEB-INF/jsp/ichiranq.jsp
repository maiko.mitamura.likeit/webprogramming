<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width" ,initial-scale=1">
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>

<header>
<title>ユーザー覧</title>
<nav class="navbar">
		<div class="container">
				<ul class="navbar-brand">${userInfo.name}さん</ul>
          		<a class="nav navbar-nav navbar-right" href="#">ログアウト</a>
			</ul>
		</div>
</header>

<body>
	<div class="container">
		<h1 class="C">ユーザ一覧</h1>
		<div align="right"><a href="ShinkiServlet">新規登録</a></div>

		<form action="" method="post">
			<form class="form-horizontal">
				<div class="form-group">
					<label for="id">ログインID</label>
					<div class="col-xs-5">
						<input id="id" type="text" name="loginId">
					</div>
				</div>
				<br> <label for="userID">ユーザ名</label><input id="userID"
					type="text" name="userID"><br>
				<br> <label>生年月日</label>
				<div class="col-xs-9 form-inline">
					<input id=birth "" type="date" name="birth" max="9999-12-31">
					<p>〜</p>
					<input type="date" name="birth" max="9999-12-31"><br>
					<input type="reset" value="reset"> <input type="submit"
						value="検索">
				</div>
			</form>
			<hr>

				<table class="table table-bordered">
					<thead>
						<tr>
							<th>ログインID</th>
							<th>ユーザ名</th>
							<th>生年月日</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<c:forEach var="user" items="${userList}">
								<tr>
									<td>${user.loginId}</td>
									<td>${user.name}</td>
									<td>${user.birthDate}</td>
									<td><a class="btn btn-primary"
										href="UserDetailServlet?id=${user.id}">詳細</a> <a
										class="btn btn-success" href="UpdateServlet?id=${user.id}">更新</a>
										<a class="btn btn-danger"
										href="DeleteServlet?id=${user.id}">削除</a></td>
								</tr>
						</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
	</div>

</body>
</html>
