<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width" ,initial-scale=1">
<meta charset="UTF-8">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<header>
	<title>ユーザ新規登録</title>
	<nav class="navbar">
		<div class="container">
			<ul class="navbar-brand">${userInfo.name}さん</ul>
			<a class="nav navbar-nav navbar-right" href="LogoutServlet">ログアウト</a>
		</div>
</header>


<body>

	<div class="container">
		<h1 class="C">ユーザ新規登録</h1>
		<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
<form action="ShinkiServlet" method="post">
<div class="container">
  <div class="row">
    <div class="col-sm-2">ログインID</div>
    <div class="col-sm-10 form-inline" style="padding: 3px;">
      <input type="text" required="required" class="form-control input-sm" name="loginId" id="loginId" placeholder="" size="20">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-2">パスワード</div>
    <div class="col-sm-10 form-inline" style="padding: 3px;">
      <input type="password" required="required" class="form-control input-sm" name="password" id="pass" placeholder="" size="20">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-2">パスワード確認</div>
    <div class="col-sm-10 form-inline" style="padding: 3px;">
      <input type="password" required="required" class="form-control input-sm" name="Rpassword" id="Rpass" placeholder="" size="20">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-2">ユーザ名</div>
    <div class="col-sm-10" style="padding: 3px;">
      <input type="text" required="required" class="form-control input-sm" name="name" id="name" placeholder="">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-2">生年月日</div>
    <div class="col-sm-10 form-inline" style="padding: 3px;">
      <input type="date" required="required" class="form-control input-sm" name="birthDate" placeholder="例:19930101" size="30">
    </div>
  </div>
  <div class="text-center" style="padding: 30px;">
　<input type="reset" value="reset"> <input type="submit" value="登録">
  </div>
</div>
			</form>
			<div align="left"><a href="UserListServlet">戻る</a></div>
</body>
</html>