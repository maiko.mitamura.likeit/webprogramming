<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width" ,initial-scale=1">
<meta charset="UTF-8">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<header>
	<title>ユーザ情報更新</title>
	<nav class="navbar">
		<div class="container">
				<ul class="navbar-brand">${userInfo.name}さん</ul>
          		<a class="nav navbar-nav navbar-right" href="#">ログアウト</a>
			</ul>
		</div>
</header>

<body>
		<div class="container">
			<h1 class="C">ユーザ情報更新</h1>
			<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
<form action="UpdateServlet" method="post">
 <input type="hidden" name="id" value="${user.id}">
  <div class="row">
    <div class="col-sm-2">ログインID</div>
    <div class="col-sm-10 form-inline" style="padding: 3px;">
      <p>${user.loginId}</p>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-2">パスワード</div>
    <div class="col-sm-10 form-inline" style="padding: 3px;">
      <input type="password"  class="form-control input-sm" name="password" id="pass" placeholder="" size="20">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-2">パスワード確認</div>
    <div class="col-sm-10 form-inline" style="padding: 3px;">
      <input type="password"  class="form-control input-sm" name="Rpassword" id="Rpass" placeholder="" size="20">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-2">ユーザ名</div>
    <div class="col-sm-10" style="padding: 3px;">
      <input type="text" required="required" class="form-control input-sm" name="name" id="name" value="${user.name}">
    </div>
  </div>
  <div class="row">
    <div class="col-sm-2">生年月日</div>
    <div class="col-sm-10 form-inline" style="padding: 3px;">
      <input type="date" required="required" class="form-control input-sm" name="birthDate" value="${user.birthDate}" size="30">
    </div>
  </div>
  <div class="text-center" style="padding: 30px;">
<input type="reset" value="reset"> <input type="submit" value="登録">
  </div></form>

<div align="left"><a href="UserListServlet">戻る</a></div>
</div>
</body>
</html>