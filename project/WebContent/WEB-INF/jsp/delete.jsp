<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width" ,initial-scale=1">
<meta charset="UTF-8">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<header>
	<title>ユーザ削除確認</title>
	<nav class="navbar">
		<div class="container">
			<ul class="navbar-brand">${userInfo.name}さん</ul>
			<a class="nav navbar-nav navbar-right" href="LogoutServlet">ログアウト</a>
		</div>
</header>

<body>
	<div class="container">
	<form action="DeleteServlet" method="post">
	 <input type="hidden" name="id" value="${user.id}">
		<h1 class="C">ユーザ削除確認</h1><br>
		<div class="text-center" style="padding: 30px;">
		<p>ログインID：${user.loginId}</p>
		<p>を本当に削除してよろしいでしょうか。</p><br>
		<input type="reset" value="reset">
		<input type="submit" value="削除"></div>
		</form>
		<div align="left"><a href="UserListServlet">戻る</a></div>
		</div>
</body>
</html>