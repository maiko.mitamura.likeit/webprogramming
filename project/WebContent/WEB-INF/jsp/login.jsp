<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<link rel="stylesheet" href="style.css">
<title>ログイン画面</title>
</head>
<body>
<div class="container">

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
<div class="C">
	<h1 class="C">ログイン画面</h1><br>
	<form action="LoginServlet" method="post">
	<input id="id" type="text" name="loginId" placeholder="ログインID"><br>
	<input id="pass" type="password" name="password" placeholder="パスワード"><br><br>
	<input type="reset" value="reset">
	<input type="submit" value="ログイン">
	</div>
	</form>
</body>
</html>