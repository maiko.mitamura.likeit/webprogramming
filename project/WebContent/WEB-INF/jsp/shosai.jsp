<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width" ,initial-scale=1">
<meta charset="UTF-8">
<link rel="stylesheet" href="style.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<header>
	<title>ユーザ情報詳細参照</title>
	<nav class="navbar">
		<div class="container">
			<ul class="navbar-brand">${userInfo.name}さん</ul>
			<a class="nav navbar-nav navbar-right" href="LogoutServlet">ログアウト</a>
		</div>
</header>

<body>
		</div>

		<div class="container">
			<h1 class="C">ユーザ情報詳細参照</h1>
  <div class="row">
    <div class="col-sm-2">ログインID</div>
    <div class="col-sm-10 form-inline" style="padding: 3px;">
      <p>${user.loginId}</p>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-2">ユーザ名</div>
    <div class="col-sm-10 form-inline" style="padding: 3px;">
    <p>${user.name}</p>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-2">生年月日</div>
    <div class="col-sm-10 form-inline" style="padding: 3px;">
    <p>${user.birthDate}</p>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-2">登録日時</div>
    <div class="col-sm-10 form-inline" style="padding: 3px;">
    <p>${user.createDate}</p>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-2">更新日時</div>
    <div class="col-sm-10 form-inline" style="padding: 3px;">
    <p>${user.updateDate}</p>
    </div>
  </div>


<div align="left"><a href="UserListServlet">戻る</a></div>
</body>
</html>