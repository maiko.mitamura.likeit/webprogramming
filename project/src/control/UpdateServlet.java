package control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo")==null) {
			response.sendRedirect("LoginServlet");
		}
		else {
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		UserDao userdao=new UserDao();
		User user = userdao.idget(id);
		request.setAttribute("user",user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);
	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

        String pass = request.getParameter("password");
        String Rpass = request.getParameter("Rpassword");
        String name = request.getParameter("name");
        String birthDate=request.getParameter("birthDate");
        String hiddenID=request.getParameter("id");

        // daoを呼ぶ
        UserDao userDao = new UserDao();

		if(pass.equals("") && Rpass.equals("")) {
			userDao.passlessupdate(name,birthDate,hiddenID);
		}
		if (!(pass.equals(Rpass))) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "確認パスワードが一致しません");
			UserDao userdao=new UserDao();
			User user = userdao.idget(hiddenID);
			request.setAttribute("user",user);
			//フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if(pass.equals("")&& !(Rpass.equals(""))) {
        	request.setAttribute("errMsg", "入力された内容は正しくありません");
        	UserDao userdao=new UserDao();
			User user = userdao.idget(hiddenID);
			request.setAttribute("user",user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;
        }
		if(Rpass.equals("")&&!(pass.equals(""))) {
        	request.setAttribute("errMsg", "入力された内容は正しくありません");
        	UserDao userdao=new UserDao();
			User user = userdao.idget(hiddenID);
			request.setAttribute("user",user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;
        }
		if(name.equals("")) {
        	request.setAttribute("errMsg", "入力された内容は正しくありません");
        	UserDao userdao=new UserDao();
			User user = userdao.idget(hiddenID);
			request.setAttribute("user",user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;
        }
		if(birthDate.equals("")) {
        	request.setAttribute("errMsg", "入力された内容は正しくありません");
        	UserDao userdao=new UserDao();
			User user = userdao.idget(hiddenID);
			request.setAttribute("user",user);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;
        }
		
		userDao.update(pass,name,birthDate,hiddenID);
		
        //ユーザーリストにリダイレクト
		response.sendRedirect("UserListServlet");
		}

}
