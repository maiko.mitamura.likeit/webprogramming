package control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDetailServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// URLからGETパラメータとしてIDを受け取る
		HttpSession session = request.getSession();
		if (session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
		} else {
			String id = request.getParameter("id");

			// 確認用：idをコンソールに出力
			System.out.println(id);

			UserDao userdao = new UserDao();
			User user = userdao.idget(id);
			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shosai.jsp");
			dispatcher.forward(request, response);

			// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する
			/*UserDao userdao=new UserDao();
			List<User> userList=userdao.findAll();
			
			request.setAttribute("userList",userList);
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shosai.jsp");
			dispatcher.forward(request, response);
			}
			
			// TODO  未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
			
			}*/
		}
	}
}
