package control;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class ShinkiServlet
 */
@WebServlet("/ShinkiServlet")
public class ShinkiServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShinkiServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo")==null) {
			response.sendRedirect("LoginServlet");
		}else {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shinki.jsp");
		dispatcher.forward(request, response);
	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");
        String loginId = request.getParameter("loginId");
        String pass = request.getParameter("password");
        String Rpass = request.getParameter("Rpassword");
        String name = request.getParameter("name");
        String birthDate=request.getParameter("birthDate");
  

        // daoを呼ぶ
        UserDao userDao = new UserDao();

        if (!(pass.equals(Rpass))) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shinki.jsp");
			dispatcher.forward(request, response);
			return;
			}
        if(pass.equals("")) {
        	request.setAttribute("errMsg", "入力された内容は正しくありません");
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shinki.jsp");
			dispatcher.forward(request, response);
			return;
        }
        if(Rpass.equals("")) {
        	request.setAttribute("errMsg", "入力された内容は正しくありません");
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shinki.jsp");
			dispatcher.forward(request, response);
			return;
        }
        if(loginId.equals("")) {
        	request.setAttribute("errMsg", "入力された内容は正しくありません");
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shinki.jsp");
			dispatcher.forward(request, response);
			return;
        }
        if(birthDate.equals("")) {
        	request.setAttribute("errMsg", "入力された内容は正しくありません");
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shinki.jsp");
			dispatcher.forward(request, response);
			return;
        }
        if(name.equals("")) {
        	request.setAttribute("errMsg", "入力された内容は正しくありません");
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shinki.jsp");
			dispatcher.forward(request, response);
			return;
        }
        


        User user = userDao.IDcheck(loginId);
        if (!(user == null)) {
    		// リクエストスコープにエラーメッセージをセット
    		request.setAttribute("errMsg", "このIDは使われています");
    		// ログインjspにフォワード
    		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/shinki.jsp");
    		dispatcher.forward(request, response);
    		return;
    		}


        userDao.insert(loginId, pass,name,birthDate);

        //ユーザーリストにリダイレクト
		response.sendRedirect("UserListServlet");


	}

}
